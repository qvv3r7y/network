package com.qvv3r7y;

import java.net.DatagramPacket;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Thread.sleep;

public class LinkManager implements Runnable {
    private static final int TIMEOUT = 5_000;
    private HashMap<String, ArrayList<Integer>> groupMembers = new HashMap<>();

    public void checkPacket(DatagramPacket packet) {
        String hostIp = packet.getAddress().getHostAddress();
        if (!groupMembers.containsKey(hostIp)) {
            ArrayList<Integer> ports = new ArrayList<>();
            ports.add(packet.getPort());
            groupMembers.put(hostIp, ports);
            System.out.println("**********\n" + groupMembers.toString() + "\n**********\n");
        } else {
            if(!groupMembers.get(hostIp).contains(packet.getPort())){
                groupMembers.get(hostIp).add(packet.getPort());
                System.out.println("**********\n" + groupMembers.toString() + "\n**********\n");
            }
        }
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                sleep(TIMEOUT);
                groupMembers.clear();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
