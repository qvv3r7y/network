package com.qvv3r7y.process;


import com.qvv3r7y.model.UniqueDestination;
import java.net.DatagramPacket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;

public class MessageRetrier implements Runnable {
    private ConcurrentHashMap<UniqueDestination, DatagramPacket> msgStorageMap;
    private DelayQueue<UniqueDestination> acknowledgeQueue;
    private Sender sender;

    public MessageRetrier(Node node) {
        this.msgStorageMap = node.getMsgStorageMap();
        this.acknowledgeQueue = node.getAcknowledgeQueue();
        this.sender = node.getSender();
    }

    public UniqueDestination checkQueue() throws InterruptedException {
        UniqueDestination ud = acknowledgeQueue.take();
        ud.resetDelay();
        acknowledgeQueue.put(ud);
        return ud;
    }

    public void retryPacket(UniqueDestination ud) {
        DatagramPacket packet = msgStorageMap.get(ud);
        sender.sendPacket(packet);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                UniqueDestination ud = checkQueue();
                retryPacket(ud);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
