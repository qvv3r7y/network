package com.qvv3r7y.model;

import java.net.InetAddress;
import java.util.Objects;

public class Direction {
    private final InetAddress ip;
    private final Integer port;

    public Direction(InetAddress ip, Integer port){
        this.port = port;
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public InetAddress getIp(){
        return ip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Direction)) return false;
        Direction that = (Direction) o;
        return Objects.equals(getIp(), that.getIp()) &&
                Objects.equals(getPort(), that.getPort());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIp(), getPort());
    }
}
