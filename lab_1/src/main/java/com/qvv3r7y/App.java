package com.qvv3r7y;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        String ip = "224.0.0.7";
        int port = 43554;

        Sender sender = new Sender(ip, port);
        Receiver receiver = new Receiver(ip, port);
        LinkManager manager = new LinkManager();

        Thread senderThr = new Thread(sender);
        Thread managerThr = new Thread(manager);

        senderThr.start();
        managerThr.start();

        while (!Thread.currentThread().isInterrupted()) {
            manager.checkPacket(receiver.receive());
        }
    }
}
