package com.qvv3r7y;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServer {
    private static long idCounter = 0;
    private static final int MAX_CLIENTS = 10;
    private int port;
    private ServerSocket socket;

    public TcpServer(String port) throws IOException {
        this.port = Integer.parseInt(port);
        this.socket = new ServerSocket(this.port, MAX_CLIENTS);
        System.out.println("* Server is running. *" +
                "\nMy ip:\t" + socket.getInetAddress().getHostAddress() +
                "\nMy port:\t" + socket.getLocalPort());
    }

    public void receiveConnection() throws IOException {
        System.out.println("* Wait connection... *");
        Socket newClientSocket = socket.accept();
        Thread clientHandler = new Thread(new ClientHandler(newClientSocket, ++idCounter));
        clientHandler.start();
        System.out.println("* Client #" + idCounter + " is connected. *" +
                "\nIp:\t" + newClientSocket.getLocalAddress().getHostAddress() +
                "\nPort:\t" + newClientSocket.getPort());
    }


    public static void main(String[] args) {
        try {
            TcpServer server = new TcpServer(args[0]);
            while (!Thread.currentThread().isInterrupted()) {
                server.receiveConnection();
            }
            server.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
