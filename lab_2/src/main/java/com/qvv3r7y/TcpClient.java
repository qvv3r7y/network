package com.qvv3r7y;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class TcpClient {
    private static final int BUFFER_SIZE = 8192;
    private Socket socket;
    private InetAddress ip;
    private int port;

    public TcpClient(String ip, String port) throws IOException {
        this.ip = InetAddress.getByName(ip);
        this.port = Integer.parseInt(port);
        this.socket = new Socket(this.ip, this.port);
    }

    public void sendFile(String path) {
        File file = new File(path);
        try (FileInputStream fileIn = new FileInputStream(file);
             DataOutputStream dataOut = new DataOutputStream(socket.getOutputStream());
             DataInputStream dataIn = new DataInputStream(socket.getInputStream())) {

            dataOut.writeUTF(file.getName()); //уже включает в себя как длину так и UTF-8 строку
            dataOut.writeLong(file.length());

            byte[] buff = new byte[BUFFER_SIZE];
            int count;

            while ((count = fileIn.read(buff)) != -1) {
                dataOut.write(buff, 0, count);
            }

            dataOut.flush();
            String confirmation = dataIn.readUTF();
            System.out.println(confirmation);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            TcpClient client = new TcpClient(args[0], args[1]);
            client.sendFile(args[2]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
