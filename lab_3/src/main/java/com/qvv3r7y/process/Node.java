package com.qvv3r7y.process;

import com.qvv3r7y.model.ANSI_COLORS;
import com.qvv3r7y.model.Direction;
import com.qvv3r7y.model.UniqueDestination;

import java.net.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.DelayQueue;

public class Node {
    private LossSimulator lossSimulator;
    private String nodeName;
    private DatagramSocket socket;
    private Direction ReplacementForNode = null;
    private Direction ReplacementForChild = null;

    private CopyOnWriteArrayList<Direction> neighboursList = new CopyOnWriteArrayList<>();
    private DelayQueue<UniqueDestination> acknowledgeQueue = new DelayQueue<>();
    private ConcurrentHashMap<UniqueDestination, DatagramPacket> msgStorageMap = new ConcurrentHashMap<>();

    private Sender sender;
    private Receiver receiver;
    private MessageRetrier retrier;
    private IntegrityController integrityController;

    public Node(String nodeName, int loss, int localPort) throws SocketException {
        this.lossSimulator = new LossSimulator(loss);
        this.nodeName = nodeName;
        this.socket = new DatagramSocket(localPort);

        sender = new Sender(this);
        integrityController = new IntegrityController(this);
        receiver = new Receiver(this);
        retrier = new MessageRetrier(this);
    }

    public Node(String nodeName, int loss, int localPort, String ip, int port) throws SocketException, UnknownHostException {
        this(nodeName, loss, localPort);
        Direction neighbour = new Direction(InetAddress.getByName(ip), port);
        neighboursList.add(neighbour);
    }

    public void start() {
        new Thread(sender).start();
        new Thread(receiver).start();
        new Thread(retrier).start();
        new Thread(integrityController).start();
    }


    public Direction getReplacementForNode() {
        return ReplacementForNode;
    }

    public void setReplacementForNode(Direction replacementForNode) {
        //System.out.println(ANSI_COLORS.ANSI_RED + "*зам ноды: " + (replacementForNode == null ? null : replacementForNode.getPort()) + "*" + ANSI_COLORS.ANSI_RESET);
        ReplacementForNode = replacementForNode;
    }

    public Direction getReplacementForChild() {
        return ReplacementForChild;
    }

    public void setReplacementForChild(Direction replacementForChild) {
        //System.out.println(ANSI_COLORS.ANSI_RED + "*зам детей: " + (replacementForChild == null ? null : replacementForChild.getPort()) + "*" + ANSI_COLORS.ANSI_RESET);
        ReplacementForChild = replacementForChild;
        sender.sendChildrenReplacement(replacementForChild);
    }

    public LossSimulator getLossSimulator() {
        return lossSimulator;
    }

    public String getNodeName() {
        return nodeName;
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public CopyOnWriteArrayList<Direction> getNeighboursList() {
        return neighboursList;
    }

    public DelayQueue<UniqueDestination> getAcknowledgeQueue() {
        return acknowledgeQueue;
    }

    public ConcurrentHashMap<UniqueDestination, DatagramPacket> getMsgStorageMap() {
        return msgStorageMap;
    }

    public Sender getSender() {
        return sender;
    }

    public IntegrityController getIntegrityController() {
        return integrityController;
    }
}
