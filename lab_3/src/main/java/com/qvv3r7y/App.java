package com.qvv3r7y;


import com.qvv3r7y.process.Node;

import java.net.SocketException;
import java.net.UnknownHostException;

public class App {
    public static void main(String[] args) {
        Node node;
        try {
            if (args.length == 3) {
                node = new Node(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
            } else if (args.length == 5) {
                node = new Node(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]),
                        args[3], Integer.parseInt(args[4]));
            } else {
                System.out.println("Bad argument, please enter this args:" +
                        "\n{ name, %loss, localPort [ipDst, portDst](opt) } ");
                return;
            }
            node.start();
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
