package com.qvv3r7y.message;

import com.qvv3r7y.model.Direction;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class ReplacementMessage extends Message {
    private Direction replacementForChild;

    public ReplacementMessage(String uid, Direction replacementForChild) {
        super(MessageType.REPLACEMENT_MSG, uid);
        this.replacementForChild = replacementForChild;
    }

    public ReplacementMessage(DatagramPacket packet) {
        super(packet);
    }

    public Direction getReplacementForChild() {
        return replacementForChild;
    }

    @Override
    public DatagramPacket createPacketFromMessage(Direction dst) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        try (DataOutputStream dout = new DataOutputStream(bout)) {
            dout.writeUTF(getType().name());
            dout.writeUTF(getUid());
            if (replacementForChild == null) {
                dout.writeBoolean(false);
            } else {
                dout.writeBoolean(true);
                dout.writeUTF(replacementForChild.getIp().getHostAddress());
                dout.writeInt(replacementForChild.getPort());
            }
            dout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] buf = bout.toByteArray();
        return new DatagramPacket(buf, buf.length, dst.getIp(), dst.getPort());
    }

    @Override
    public void initMessageFromPacket(DatagramPacket packet) {
        byte[] buf = packet.getData();
        ByteArrayInputStream bin = new ByteArrayInputStream(buf);

        try (DataInputStream din = new DataInputStream(bin)) {
            MessageType type = MessageType.valueOf(din.readUTF());
            String uid = din.readUTF();
            setType(type);
            setUid(uid);
            if (din.readBoolean()) {
                InetAddress ip = InetAddress.getByName(din.readUTF());
                int port = din.readInt();
                replacementForChild = new Direction(ip, port);
            } else {
                replacementForChild = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
