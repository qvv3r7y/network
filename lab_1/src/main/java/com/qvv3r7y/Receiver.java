package com.qvv3r7y;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Receiver {
    public static final int BUF_SIZE = 1024;
    private final InetAddress multicastIp;
    private final int port;
    private final MulticastSocket socket;
    private byte[] buf;

    public Receiver(String multicastIp, int port) throws IOException {
        this.port = port;
        this.buf = new byte[BUF_SIZE];
        this.socket = new MulticastSocket(port);
        this.multicastIp = InetAddress.getByName(multicastIp);
        socket.joinGroup(this.multicastIp);
    }


    public DatagramPacket receive() throws IOException {
        DatagramPacket packet = new DatagramPacket(buf, BUF_SIZE);
        socket.receive(packet);
        return packet;
    }

    public void leaveGroup() throws IOException {
        socket.leaveGroup(multicastIp);
        socket.close();
    }
}
