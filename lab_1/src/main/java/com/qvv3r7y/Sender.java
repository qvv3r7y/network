package com.qvv3r7y;

import java.io.IOException;
import java.net.*;

import static java.lang.Thread.sleep;

public class Sender implements Runnable {
    private static final int PERIOD = 1000;
    private final DatagramSocket socket = new DatagramSocket();
    private final InetAddress multicastIp;
    private final int port;

    Sender(String multicastIp, int port) throws UnknownHostException, SocketException {
        this.multicastIp = InetAddress.getByName(multicastIp);
        this.port = port;
    }

    @Override
    public void run() {
        byte[] buf = "HELLO NETWORK".getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, multicastIp, port);
        try {
            while (!Thread.currentThread().isInterrupted()) {
                socket.send(packet);
                sleep(PERIOD);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
