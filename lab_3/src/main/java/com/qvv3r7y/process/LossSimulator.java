package com.qvv3r7y.process;

import com.qvv3r7y.model.ANSI_COLORS;

import java.util.Random;

public class LossSimulator {
    private static final int MAX_BOUND_LOSS = 100;
    private Random random = new Random(System.currentTimeMillis());
    private final int loss;

    public LossSimulator(int loss) {
        this.loss = loss;
    }

    public boolean isLost() {
        int x = random.nextInt(MAX_BOUND_LOSS);

        if (x < loss) System.out.println(ANSI_COLORS.ANSI_RED + "*ПОТЕРЯЛСЯ*" + ANSI_COLORS.ANSI_RESET);

        return (x < loss);
    }
}
