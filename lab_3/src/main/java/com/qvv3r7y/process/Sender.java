package com.qvv3r7y.process;

import com.qvv3r7y.message.HelloMessage;
import com.qvv3r7y.message.Message;
import com.qvv3r7y.message.ReplacementMessage;
import com.qvv3r7y.message.TextMessage;
import com.qvv3r7y.model.ANSI_COLORS;
import com.qvv3r7y.model.Direction;
import com.qvv3r7y.model.UniqueDestination;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.DelayQueue;

public class Sender implements Runnable {
    private String nodeName;
    private DatagramSocket socket;
    private CopyOnWriteArrayList<Direction> neighboursList;
    private ConcurrentHashMap<UniqueDestination, DatagramPacket> msgStorageMap;
    private DelayQueue<UniqueDestination> acknowledgeQueue;
    private Node node;

    public Sender(Node node) {
        this.nodeName = node.getNodeName();
        this.socket = node.getSocket();
        this.msgStorageMap = node.getMsgStorageMap();
        this.acknowledgeQueue = node.getAcknowledgeQueue();
        this.neighboursList = node.getNeighboursList();
        this.node = node;
    }

    public void sendPacket(DatagramPacket packet) {
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Message msg, Direction dst, boolean requestAck) {
        DatagramPacket packet = msg.createPacketFromMessage(dst);
        if (requestAck) {
            UniqueDestination ud = new UniqueDestination(msg.getUid(), dst);
            acknowledgeQueue.put(ud);
            msgStorageMap.put(ud, packet);
        }
        sendPacket(packet);
    }

    public void sendMessageAllNeighbours(Message msg, boolean requestAck) {
        for (Direction dst : neighboursList) {
            sendMessage(msg, dst, requestAck);
        }
    }

    public void forwardMessageAllNeighbours(Message msg, Direction except, boolean requestAck) {
        for (Direction dst : neighboursList) {
            if (dst.equals(except)) {
                continue;
            }
            sendMessage(msg, dst, requestAck);
        }
    }

    public void connectToNode(Direction dst) {
        Message helloMsg = new HelloMessage(UUID.randomUUID().toString(), nodeName);
        sendMessage(helloMsg, dst, true);
        node.setReplacementForChild(dst);
        if(neighboursList.isEmpty() || !neighboursList.get(0).equals(dst)){
            neighboursList.add(dst);
        }
    }

    public void sendChildrenReplacement(Direction replacement) {
        Message rplMsg = new ReplacementMessage(UUID.randomUUID().toString(), replacement);
        forwardMessageAllNeighbours(rplMsg, replacement, false);
    }

    @Override
    public void run() {
        System.out.println(ANSI_COLORS.ANSI_PURPLE + "Hi, " + nodeName + "." + ANSI_COLORS.ANSI_RESET);
        if (!neighboursList.isEmpty()) {
            System.out.println(ANSI_COLORS.ANSI_PURPLE + "Подключение к чату..." + ANSI_COLORS.ANSI_RESET);
            connectToNode(neighboursList.get(0));
        } else {
            System.out.println(ANSI_COLORS.ANSI_PURPLE + "Пригласите друзей в чат!" + ANSI_COLORS.ANSI_RESET);
        }
        Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            String text = scanner.nextLine();
            if (text.equals("EXIT")) {
                Thread.currentThread().interrupt();
                scanner.close();
                socket.close();
                continue;
            }
            Message msg = new TextMessage(UUID.randomUUID().toString(), nodeName, text);
            sendMessageAllNeighbours(msg, true);
        }
        System.exit(0);
    }
}
