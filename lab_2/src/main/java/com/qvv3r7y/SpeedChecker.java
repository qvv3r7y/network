package com.qvv3r7y;

public class SpeedChecker {
    private final long PRINT_TIMEOUT = 3000;
    private final long CLIENT_ID;

    private long start;
    private long checkpointTime;
    private long byteRead;
    private long checkpointByte;

    private int currentSpeed;
    private int sessionSpeed;

    public SpeedChecker(long clientId) {
        this.CLIENT_ID = clientId;
    }

    public void start() {
        this.reset();
        this.start = System.currentTimeMillis();
        this.checkpointTime = this.start;
    }

    void checkSpeed(int countReadBytes) {
        byteRead += countReadBytes;
        long currentTime = System.currentTimeMillis();

        if (currentTime - checkpointTime >= PRINT_TIMEOUT) {
            calculateSpeed(currentTime);
            printSpeed();
        }
    }

    private void calculateSpeed(long currentTime) {
        currentSpeed = (int) ((byteRead - checkpointByte) / (currentTime - checkpointTime + 1));
        sessionSpeed = (int) (byteRead / (currentTime - start + 1));

        this.checkpointTime = currentTime;
        this.checkpointByte = byteRead;
    }

    public void printSpeed() {
        System.out.println("_________________________________________" +
                "\nClient #" + CLIENT_ID + ":" +
                "\nCurrent Speed:\t" + currentSpeed + " KB/Sec" +
                "\nSession Speed:\t" + sessionSpeed + " KB/Sec" +
                "\n''''''''''''''''''''''''''''''''''''''''");
    }

    public void finish() {
        calculateSpeed(System.currentTimeMillis());
        printSpeed();
    }

    public void reset() {
        this.checkpointTime = 0;
        this.checkpointByte = 0;
        this.byteRead = 0;
    }
}
