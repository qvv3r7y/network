package com.qvv3r7y.model;

import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class UniqueDestination implements Delayed {
    private static final long ACK_DELAY = 3000;
    private String uid;
    private Direction dst;
    private long startTime;

    public UniqueDestination(String uid, Direction dst) {
        this.uid = uid;
        this.dst = dst;
        this.startTime = System.currentTimeMillis() + ACK_DELAY;
    }

    public String getUid() {
        return uid;
    }

    public Direction getDst() {
        return dst;
    }

    public void resetDelay() {
        startTime = System.currentTimeMillis() + ACK_DELAY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UniqueDestination)) return false;
        UniqueDestination that = (UniqueDestination) o;
        return Objects.equals(getUid(), that.getUid()) &&
                Objects.equals(getDst(), that.getDst());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUid(), getDst());
    }

    @Override
    public long getDelay(TimeUnit timeUnit) {
        long diff = startTime - System.currentTimeMillis();
        return timeUnit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed delayed) {
        return (int) (this.startTime - ((UniqueDestination) delayed).startTime);
    }
}
