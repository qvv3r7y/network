package com.qvv3r7y.message;

import com.qvv3r7y.model.Direction;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class AcknowledgeMessage extends Message {
    private Direction neighbour;

    public AcknowledgeMessage(String uid) {
        super(MessageType.ACK_MSG, uid);
    }

    public AcknowledgeMessage(DatagramPacket packet) {
        super(packet);
    }

    public Direction getNeighbour() {
        return neighbour;
    }

    @Override
    public DatagramPacket createPacketFromMessage(Direction dst) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        try (DataOutputStream dout = new DataOutputStream(bout)) {
            dout.writeUTF(getType().name());
            dout.writeUTF(getUid());
            dout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] buf = bout.toByteArray();
        return new DatagramPacket(buf, buf.length, dst.getIp(), dst.getPort());
    }

    @Override
    public void initMessageFromPacket(DatagramPacket packet) {
        byte[] buf = packet.getData();
        ByteArrayInputStream bin = new ByteArrayInputStream(buf);

        try (DataInputStream din = new DataInputStream(bin)) {
            MessageType type = MessageType.valueOf(din.readUTF());
            String uid = din.readUTF();
            setType(type);
            setUid(uid);
            InetAddress ip = packet.getAddress();
            int port = packet.getPort();
            neighbour = new Direction(ip, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
