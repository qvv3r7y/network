package com.qvv3r7y.message;

import com.qvv3r7y.model.Direction;

import java.io.*;
import java.net.DatagramPacket;

public class TextMessage extends Message {
    private String srcName;
    private String text;

    public TextMessage(String uid, String srcName, String text) {
        super(MessageType.TEXT_MSG, uid);
        this.srcName = srcName;
        this.text = text;
    }

    public TextMessage(DatagramPacket packet) {
        super(packet);
    }

    public String getSrcName() {
        return srcName;
    }

    public String getText() {
        return text;
    }

    @Override
    public void initMessageFromPacket(DatagramPacket packet) {
        byte[] buf = packet.getData();
        ByteArrayInputStream bin = new ByteArrayInputStream(buf);

        try (DataInputStream din = new DataInputStream(bin)) {
            MessageType type = MessageType.valueOf(din.readUTF());
            String uid = din.readUTF();
            setType(type);
            setUid(uid);
            srcName = din.readUTF();
            text = din.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public DatagramPacket createPacketFromMessage(Direction dst) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        try (DataOutputStream dout = new DataOutputStream(bout)) {
            dout.writeUTF(getType().name());
            dout.writeUTF(getUid());
            dout.writeUTF(srcName);
            dout.writeUTF(text);
            dout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] buf = bout.toByteArray();
        return new DatagramPacket(buf, buf.length, dst.getIp(), dst.getPort());
    }
}
