package com.qvv3r7y.message;

public enum MessageType {
    HELLO_MSG,
    ACK_MSG,
    TEXT_MSG,
    PING_MSG,
    REPLACEMENT_MSG
}
