package com.qvv3r7y;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {
    private static final int BUFFER_SIZE = 8192;
    private static final String PATH = "uploads";
    private Socket socket;

    private SpeedChecker speedChecker;
    private final long clientId;


    public ClientHandler(Socket socket, long clientId) {
        this.socket = socket;
        this.clientId = clientId;
        this.speedChecker = new SpeedChecker(clientId);
    }

    public void receiveFile() {
        try (DataInputStream dataIn = new DataInputStream(socket.getInputStream());
             DataOutputStream dataOut = new DataOutputStream(socket.getOutputStream())) {

            String fileName = dataIn.readUTF();
            long fileSize = dataIn.readLong();
            FileOutputStream fileOut = new FileOutputStream(PATH + File.separator + fileName);

            byte[] buff = new byte[BUFFER_SIZE];
            long leftBytes = fileSize;

            speedChecker.start();

            while (leftBytes > 0) {
                int nextChunk = (int) Math.min(leftBytes, BUFFER_SIZE);
                int count = dataIn.read(buff, 0, nextChunk);
                if (count == -1) {
                    throw new RuntimeException("Bad qty bytes was read");
                }
                fileOut.write(buff, 0, count);
                leftBytes -= count;
                speedChecker.checkSpeed(count);
            }

            speedChecker.finish();

            if (leftBytes != 0) {
                throw new RuntimeException("Bad qty bytes was read");
            }

            dataOut.writeUTF("File transferred successfully");
            fileOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        receiveFile();
    }
}
