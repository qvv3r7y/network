package com.qvv3r7y.process;

import com.qvv3r7y.message.PingMessage;
import com.qvv3r7y.model.ANSI_COLORS;
import com.qvv3r7y.model.Direction;
import com.qvv3r7y.model.UniqueDestination;

import java.net.DatagramPacket;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.DelayQueue;

public class IntegrityController implements Runnable {
    private static final int PING_PERIOD = 5000;
    private LinkedList<Direction> disconnectNeighbours;
    private CopyOnWriteArrayList<Direction> neighboursList;
    private ConcurrentHashMap<UniqueDestination, DatagramPacket> msgStorageMap;
    private DelayQueue<UniqueDestination> acknowledgeQueue;
    private Sender sender;
    private Node node;

    public IntegrityController(Node node) {
        this.msgStorageMap = node.getMsgStorageMap();
        this.acknowledgeQueue = node.getAcknowledgeQueue();
        this.neighboursList = node.getNeighboursList();
        this.sender = node.getSender();
        this.node = node;
    }


    private void pingNeighbours() {
        disconnectNeighbours = new LinkedList<>(neighboursList);
        PingMessage msg = new PingMessage(UUID.randomUUID().toString(), false);
        for (Direction neighbour : disconnectNeighbours) {
            sender.sendMessage(msg, neighbour, false);
        }
    }

    public void putPingAnswer(Direction neighbour) {
        disconnectNeighbours.remove(neighbour);
    }

    private void disconnectNeighbour() throws InterruptedException {
        if (!disconnectNeighbours.isEmpty()) {
            Direction disconnected = disconnectNeighbours.pop();
            neighboursList.remove(disconnected);
            clearAcknowledgeQueue(disconnected);
            System.out.println(ANSI_COLORS.ANSI_RED + "*Участник покинул встречу* " + disconnected.getPort() + ANSI_COLORS.ANSI_RESET);
            if (disconnected.equals(node.getReplacementForChild())) {
                node.setReplacementForChild(null);
                if (node.getReplacementForNode() != null) {
                    Thread.sleep(PING_PERIOD);
                    sender.connectToNode(node.getReplacementForNode());
                }
            }
            System.out.println(ANSI_COLORS.ANSI_RED + "*Целостность восстановленна*" + ANSI_COLORS.ANSI_RESET);
        }
    }

    public void clearAcknowledgeQueue(Direction disconnected) {
        for (UniqueDestination ud : acknowledgeQueue) {
            if (ud.getDst().equals(disconnected)) {
                acknowledgeQueue.remove(ud);
                msgStorageMap.remove(ud);
            }
        }
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                pingNeighbours();
                Thread.sleep(PING_PERIOD);
                disconnectNeighbour();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
