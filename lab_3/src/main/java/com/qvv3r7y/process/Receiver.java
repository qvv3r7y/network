package com.qvv3r7y.process;

import com.qvv3r7y.message.*;
import com.qvv3r7y.model.ANSI_COLORS;
import com.qvv3r7y.model.Direction;
import com.qvv3r7y.model.UniqueDestination;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.DelayQueue;

public class Receiver implements Runnable {
    private static final int BUF_SIZE = 8192;
    private LossSimulator lossSimulator;
    private DatagramSocket socket;
    private CopyOnWriteArrayList<Direction> neighboursList;
    private ConcurrentHashMap<UniqueDestination, DatagramPacket> msgStorageMap;
    private DelayQueue<UniqueDestination> acknowledgeQueue;
    private Sender sender;
    private IntegrityController integrityController;
    private Node node;

    public Receiver(Node node) {
        this.socket = node.getSocket();
        this.msgStorageMap = node.getMsgStorageMap();
        this.acknowledgeQueue = node.getAcknowledgeQueue();
        this.neighboursList = node.getNeighboursList();
        this.sender = node.getSender();
        this.lossSimulator = node.getLossSimulator();
        this.integrityController = node.getIntegrityController();
        this.node = node;
    }

    private void receive() throws IOException {
        byte[] buf = new byte[BUF_SIZE];
        DatagramPacket packet = new DatagramPacket(buf, BUF_SIZE);
        socket.receive(packet);
        switch (Message.getTypeFromPacket(packet)) {
            case HELLO_MSG:
                if (!lossSimulator.isLost()) {
                    handleHelloMessage(packet);
                }
                break;
            case TEXT_MSG:
                if (!lossSimulator.isLost()) {
                    handleTextMessage(packet);
                }
                break;
            case ACK_MSG:
                handleAckMessage(packet);
                break;
            case REPLACEMENT_MSG:
                handleReplacementMessage(packet);
                break;
            case PING_MSG:
                handlePingMessage(packet);
                break;
        }
    }

    private void handlePingMessage(DatagramPacket packet) {
        PingMessage msg = new PingMessage(packet);
        if (msg.isAnswer()) {
            integrityController.putPingAnswer(msg.getSrc());
        } else {
            PingMessage answer = new PingMessage(UUID.randomUUID().toString(), true);
            sender.sendMessage(answer, msg.getSrc(), false);
        }
    }

    private void confirmReceipt(String uidReceivedMsg, DatagramPacket packet) {
        Direction neighbour = new Direction(packet.getAddress(), packet.getPort());
        AcknowledgeMessage msg = new AcknowledgeMessage(uidReceivedMsg);
        sender.sendMessage(msg, neighbour, false);
    }

    private void handleTextMessage(DatagramPacket packet) {
        TextMessage msg = new TextMessage(packet);
        confirmReceipt(msg.getUid(), packet);
        String text = msg.getText();
        String srcName = msg.getSrcName();
        System.out.println(ANSI_COLORS.ANSI_BLUE + "[" + srcName + "]: " + ANSI_COLORS.ANSI_CYAN + text + ANSI_COLORS.ANSI_RESET);
        sender.forwardMessageAllNeighbours(msg, new Direction(packet.getAddress(), packet.getPort()), true);
    }

    private void handleHelloMessage(DatagramPacket packet) {
        HelloMessage msg = new HelloMessage(packet);
        confirmReceipt(msg.getUid(), packet);
        Message rplMsg = new ReplacementMessage(UUID.randomUUID().toString(), node.getReplacementForChild());

        sender.sendMessage(rplMsg, msg.getNeighbour(), false);
        if (node.getReplacementForChild() == null) {
            node.setReplacementForChild(msg.getNeighbour());
        }

        neighboursList.add(msg.getNeighbour());
        System.out.println(ANSI_COLORS.ANSI_PURPLE + "************************************" +
                "\n>>>" + msg.getSrcName() + "<<< - Ваш новый сосед" +
                "\n************************************" + ANSI_COLORS.ANSI_RESET);
    }

    private void handleAckMessage(DatagramPacket packet) {
        AcknowledgeMessage msg = new AcknowledgeMessage(packet);
        Direction src = msg.getNeighbour();
        String uid = msg.getUid();
        UniqueDestination ud = new UniqueDestination(uid, src);
        acknowledgeQueue.remove(ud);
        msgStorageMap.remove(ud);
    }

    public void handleReplacementMessage(DatagramPacket packet) {
        ReplacementMessage msg = new ReplacementMessage(packet);
        Direction replacement = msg.getReplacementForChild();
        node.setReplacementForNode(replacement);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                receive();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
