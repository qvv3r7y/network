package com.qvv3r7y.message;

import com.qvv3r7y.model.Direction;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class PingMessage extends Message {
    private Direction src;
    private boolean answer;

    public PingMessage(String uid, boolean answer) {
        super(MessageType.PING_MSG, uid);
        this.answer = answer;
    }

    public PingMessage(DatagramPacket packet) {
        super(packet);
    }

    public Direction getSrc() {
        return src;
    }

    public boolean isAnswer() {
        return answer;
    }


    @Override
    public DatagramPacket createPacketFromMessage(Direction dst) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        try (DataOutputStream dout = new DataOutputStream(bout)) {
            dout.writeUTF(getType().name());
            dout.writeUTF(getUid());
            dout.writeBoolean(answer);
            dout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] buf = bout.toByteArray();
        return new DatagramPacket(buf, buf.length, dst.getIp(), dst.getPort());
    }

    @Override
    public void initMessageFromPacket(DatagramPacket packet) {
        byte[] buf = packet.getData();
        ByteArrayInputStream bin = new ByteArrayInputStream(buf);

        try (DataInputStream din = new DataInputStream(bin)) {
            MessageType type = MessageType.valueOf(din.readUTF());
            String uid = din.readUTF();
            setType(type);
            setUid(uid);
            this.answer = din.readBoolean();
            InetAddress ip = packet.getAddress();
            int port = packet.getPort();
            src = new Direction(ip, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
