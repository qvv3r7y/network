package com.qvv3r7y.message;

import com.qvv3r7y.model.Direction;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.DatagramPacket;

public abstract class Message {
    private MessageType type;
    private String uid;

    public Message(MessageType type, String uid) {
        this.type = type;
        this.uid = uid;
    }

    public Message(DatagramPacket packet) {
        initMessageFromPacket(packet);
    }

    protected void setType(MessageType type) {
        this.type = type;
    }

    protected void setUid(String uid) {
        this.uid = uid;
    }

    public MessageType getType() {
        return type;
    }

    public String getUid() {
        return uid;
    }

    public abstract DatagramPacket createPacketFromMessage(Direction dst);

    public abstract void initMessageFromPacket(DatagramPacket packet);

    public static MessageType getTypeFromPacket(DatagramPacket packet) {
        MessageType type = null;
        byte[] buf = packet.getData();

        ByteArrayInputStream bin = new ByteArrayInputStream(buf);
        try (DataInputStream din = new DataInputStream(bin)) {
            type = MessageType.valueOf(din.readUTF());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return type;
    }
}
